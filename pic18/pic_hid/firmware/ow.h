/* Copyright � 2011 Demetris Stavrou
 * version 1.0
 * ---------------------------------
 * OneWire Library for the C18 Compiler
 * Only the basic functions are included
 * ow_reset() for OneWire Reset
 * ow_write_byte(char) for writting byte
 * char ow_read_byte() for reading byte
 *
 * Just include the ow.h file in your project and define three required
 * names - OW_LAT, OW_PIN, OW_TRIS
 *
 * You can use this library for interfacing the Maxim thermometers like DS18S20
 * NOTE: This library works for 40MHz Frequency i.e. TCY = 0.1us
 * You can adjust the timings accordingly, at least for now!
 */
#ifndef __OW_H
#define __OW_H
/* -------------------------------------------------------------------------- */
/*                            DS1820 Commands                                 */
/* -------------------------------------------------------------------------- */

#define DS1820_CMD_SEARCHROM     0xF0
#define DS1820_CMD_READROM       0x33
#define DS1820_CMD_MATCHROM      0x55
#define DS1820_CMD_SKIPROM       0xCC
#define DS1820_CMD_ALARMSEARCH   0xEC
#define DS1820_CMD_CONVERTTEMP   0x44
#define DS1820_CMD_WRITESCRPAD   0x4E
#define DS1820_CMD_READSCRPAD    0xBE
#define DS1820_CMD_COPYSCRPAD    0x48
#define DS1820_CMD_RECALLEE      0xB8
#define DS1820_FAMILY_CODE_DS18B20      0x28
#define DS1820_FAMILY_CODE_DS18S20      0x10
#define OW_LAT LATCbits.LATC1
#define OW_PIN PORTCbits.RC1
#define OW_TRIS TRISCbits.TRISC1
#include <delays.h>
#ifndef OW_LAT
#error "Define the LAT address of the DQ pin (example:LATCbits.LATC1) for writting"
#endif

#ifndef OW_PIN
#error "Define the PORT address of the DQ pin (example:PORTCbits.RC1) for reading"
#endif

#ifndef OW_TRIS
#error "Define the TRIS address of the DQ pin (example:TRISCbits.TRISC1)"
#endif

#define OUTPUT 0
#define INPUT 1

unsigned char ow_reset(void)
{
    short device_found=0;
    // DQ High
    OW_TRIS=INPUT;
    // DQ Low
    OW_LAT=0;
    OW_TRIS=OUTPUT;
    // delay 480us
    Delay100TCYx(48);
    // DQ High
    OW_TRIS=INPUT;
    // Wait for the sensors to respond
    Delay100TCYx(6);
    // Determine is a device has responded
    device_found = !OW_PIN;
    return device_found;
}

void ow_write_byte(unsigned char data)
{
    char i;
    for (i=0;i<8;i++)
    {
        // DQ Low
        OW_LAT=0;
        OW_TRIS=OUTPUT;
        // Keep it low for 10us to start the WRITE
        Delay10TCYx(10);
        // Keep low i.e. keep output mode and low if WRITE0
        // or release line i.e. make input to take it high if WRITE1
        OW_TRIS = data & 0x01;
        Delay10TCYx(50);
        // Release the line. Total of 60us
        OW_TRIS=INPUT;
        // Some recovery time between the bits 2us
        Delay10TCYx(2);
        data =data >>1;
    }
}

char ow_read_byte(void)
{
    char data=0;
    char i;
    for (i=0;i<8;i++)
    {
        // DQ Low. Initiate READ
        OW_LAT=0;
        OW_TRIS=OUTPUT;
        Delay10TCYx(2);
        // DQ High. Release
        OW_TRIS=INPUT;
        // Wait for total of 10 us since READ initiation
        Delay10TCYx(8);
        // Read the bit and place it in the data at the right position
        data = (OW_PIN << i) | data;
        // Additional time to complete the minimum 60 us READ cycle
        Delay10TCYx(50);
    }
    return data;
}
#endif