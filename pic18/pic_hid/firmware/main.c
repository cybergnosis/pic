/************************************************************************
	main.c

    usbGenericHidCommunication reference firmware 3_0_0_0
    Copyright (C) 2011 Simon Inns

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Email: simon.inns@gmail.com

************************************************************************/

#ifndef MAIN_C
#define MAIN_C

// Global includes
#include <stdio.h>
#include <usart.h>
// Note: string.h is required for sprintf commands for debug
#include <string.h>
#include <p18cxxx.h>
#include	<p18F4455.h>					// Register definitions
// Local includes
#include "HardwareProfile.h"
#include "ow.h"
#define CLK_48MHZ 1

// Microchip Application Library includes
// (expects V2.9a of the USB library from "Microchip Solutions v2011-07-14")
//
// The library location must be set in:
// Project -> Build Options Project -> Directories -> Include search path
// in order for the project to compile.
#include "./USB/usb.h"
#include "./USB/usb_function_hid.h"
#define DEBUG_PRINT
// Ensure we have the correct target PIC device family
#if !defined(__18F4455_H) && !defined(__18F2550_H)
    #error "This firmware only supports either the PIC18F4550 or PIC18F2550 microcontrollers."
#endif

// Define the globals for the USB data in the USB RAM of the PIC18F*550
#pragma udata
#pragma udata USB_VARIABLES=0x500
unsigned char ReceivedDataBuffer[64];
unsigned char ToSendDataBuffer[64];
#pragma udata

USB_HANDLE USBOutHandle = 0;
USB_HANDLE USBInHandle = 0;
BOOL blinkStatusValid = FLAG_TRUE;

// PIC18F4550/PIC18F2550 configuration for the WFF Generic HID test device
#pragma config PLLDIV   =3         // 20Mhz external oscillator
#pragma config CPUDIV   = OSC1_PLL2
#pragma config USBDIV   = 2         // Clock source from 96MHz PLL/2
#pragma config FOSC     = HSPLL_HS
#pragma config FCMEN    = OFF
#pragma config IESO     = OFF
#pragma config PWRT     = ON
#pragma config BOR      = ON
#pragma config BORV     = 3
#pragma config VREGEN   = ON
#pragma config WDT      = OFF
#pragma config WDTPS    = 32768
#pragma config MCLRE    = ON
#pragma config LPT1OSC  = OFF
#pragma config PBADEN   = OFF
#pragma config STVREN   = ON
#pragma config LVP      = OFF
#pragma config XINST    = OFF
#pragma config CP0      = OFF
#pragma config CP1      = OFF
#pragma config CPB      = OFF
#pragma config WRT0     = OFF
#pragma config WRT1     = OFF
#pragma config WRTB     = OFF
#pragma config WRTC     = OFF
#pragma config EBTR0    = OFF
#pragma config EBTR1    = OFF
#pragma config EBTRB    = OFF

#define MAX_LED_VALUE 12
#define LED0              LATAbits.LATA0
#define LED1              LATAbits.LATA1
#define LED2              LATAbits.LATA2
#define LED3              LATAbits.LATA3
#define LED4              LATAbits.LATA5
#define LED5              LATBbits.LATB0
#define LED6              LATBbits.LATB1
#define LED7              LATBbits.LATB2
#define LED8              LATBbits.LATB3
#define LED9              LATBbits.LATB4
#define LED10             LATBbits.LATB5
#define LED11             LATBbits.LATB6
#define LED12             LATBbits.LATB7

// Private function prototypes
static void initialisePic(void);
void processUsbCommands(void);
void applicationInit(void);
void USBCBSendResume(void);
void highPriorityISRCode();
void lowPriorityISRCode();

// Remap vectors for compatibilty with Microchip USB boot loaders
#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x1000
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x1008
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x1018
#elif defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x800
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x808
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x818
#else
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x00
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x08
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x18
#endif

#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER) || defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	extern void _startup (void);
	#pragma code REMAPPED_RESET_VECTOR = REMAPPED_RESET_VECTOR_ADDRESS
	void _reset (void)
	{
	    _asm goto _startup _endasm
	}
#endif

#pragma code REMAPPED_HIGH_INTERRUPT_VECTOR = REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS
void Remapped_High_ISR (void)
{
     _asm goto highPriorityISRCode _endasm
}

#pragma code REMAPPED_LOW_INTERRUPT_VECTOR = REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS
void Remapped_Low_ISR (void)
{
     _asm goto lowPriorityISRCode _endasm
}

#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER) || defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
#pragma code HIGH_INTERRUPT_VECTOR = 0x08
void High_ISR (void)
{
     _asm goto REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS _endasm
}

#pragma code LOW_INTERRUPT_VECTOR = 0x18
void Low_ISR (void)
{
     _asm goto REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS _endasm
}
#endif

#pragma code

// High-priority ISR handling function
#pragma interrupt highPriorityISRCode
void highPriorityISRCode()
{
	// Application specific high-priority ISR code goes here

	#if defined(USB_INTERRUPT)
		// Perform USB device tasks
		USBDeviceTasks();
	#endif

}

// Low-priority ISR handling function
#pragma interruptlow lowPriorityISRCode
void lowPriorityISRCode()
{
	// Application specific low-priority ISR code goes here
}

// Firmware global variables
// Success indication globals (used to keep the success LED on so you can see it)
INT32 successIndicatorCounter = 0;
UINT8 successIndicatorFlag = FLAG_FALSE;

// Failure indication globals (used to keep the failure LED on so you can see it)
INT32 failureIndicatorCounter = 0;
UINT8 failureIndicatorFlag = FLAG_FALSE;

// Variables required for keeping track of a bulk receive command
UINT8 bulkReceiveFlag = FLAG_FALSE;
INT bulkReceivePacketCounter = 0;
INT bulkReceiveExpectedPackets = 0;

// Variables required for keeping track of a bulk send command
UINT8 bulkSendFlag = FLAG_FALSE;
INT bulkSendPacketCounter = 0;
INT bulkSendExpectedPackets = 0;

// String for creating debug messages
char debugString[64];

static void TxRS232(unsigned char txData)
{
    _asm clrwdt _endasm;
    _usart_putc(txData);
}

static void InitializeUSART()
{
    TRISC &= 0xBF; // Set RC6 as an output
    TRISC |= 0x80; // Set RC7 as an input
    RCSTA   = 0x90; // Enable serial port, enable receiver
    TXSTA   = 0x24; // Asynch, TSR empty, BRGH=1

    // Baud rate formula for BRG16=1, BRGH=1: Baud Rate = Fosc/(4 (n + 1)),
    // or n = (Fosc / (4 * BaudRate)) - 1
    // At 48 MHz, for 115.2K Baud:
    //SPBRGH:SPBRG = n = Fosc / (4 * 115200) - 1 = 103.17
    BAUDCON = 0;//0x18; // BRG16=1 txd inverted
    SPBRGH  = 0x00; // At 48MHz, SPBRGH=0, SPBRG=103 gives 115.2K Baud
    //SPBRGH  = 0x02; // 0x0270 gives 19200 Baud
#if CLK_48MHZ
//    SPBRG   = 103;  // For 48 MHz clock
    SPBRG   =25;//0x70;  // For 48 MHz clock
#else
    SPBRG   = 52;   // For 24 MHz clock
#endif
    printf("USB Test Startup\r\n");
}
#endif

// Main program entry point
void main(void)
{
   // byte led_cnt = 0;   
   // Initialise and configure the PIC ready to go
   initialisePic();
   /* UCON = 0;
      UIE = 0;
      UCFG = 0x14; // Enable pullup resistors; full speed mode    ;
      UCONbits.USBEN = 1;     */
	// If we are running in interrupt mode attempt to attach the USB device
    #if defined(USB_INTERRUPT)
        USBDeviceAttach();
    #endif
    // Show that we are up and running
    //mStatusLED0_on();

#if defined(DEBUG_PRINT)
	printf( "USB Device Initialised.\r\n");
#endif
	// Main processing loop
    while(1)
    {     
        #if defined(USB_POLLING)
			// If we are in polling mode the USB device tasks must be processed here
			// (otherwise the interrupt is performing this task)
	        USBDeviceTasks();
        #endif
    	// Process USB Commands
        processUsbCommands();
        // Note: Other application specific actions can be placed here
    }
}

// Initialise the PIC
static void initialisePic(void)
{
    // PIC port set up --------------------------------------------------------
	// Default all pins to digital
    ADCON1 = 0x0F;
	// Configure ports as inputs (1) or outputs(0)    
	TRISA = 0b11010000;
	TRISB = 0b00000000;
	TRISC = 0b111111111;
#if defined(__18F4455)
	TRISD = 0b11111111;
	TRISE = 0b11111000;
#endif
	// Clear all ports    
	PORTA = 0b00101110;
	PORTB = 0b11111111;
	PORTC = 0b00000000;
#if defined(__18F4455)
	PORTD = 0b00000000;
	PORTE = 0b00000111;
#endif
 #if defined(DEBUG_PRINT)
	InitializeUSART();
#endif
    T0CON=0x80;
	// If you have a VBUS sense pin (for self-powered devices when you
	// want to detect if the USB host is connected) you have to specify
	// your input pin in HardwareProfile.h
    #if defined(USE_USB_BUS_SENSE_IO)
    	tris_usb_bus_sense = INPUT_PIN;
    #endif
    // In the case of a device which can be both self-powered and bus-powered
    // the device must respond correctly to a GetStatus (device) request and
    // tell the host how it is currently powered.
    //
    // To do this you must device a pin which is high when self powered and low
    // when bus powered and define this in HardwareProfile.h
    #if defined(USE_SELF_POWER_SENSE_IO)
    	tris_self_power = INPUT_PIN;
    #endif
    // Application specific initialisation
   applicationInit();
    // Initialise the USB device
    USBDeviceInit();
}

// Application specific device initialisation
void applicationInit(void)
{
    // Initialize the variable holding the USB handle for the last transmission
    USBOutHandle = 0;
    USBInHandle = 0;
}

// Process USB commands
void processUsbCommands(void)
{
	// Note: For all tests we expect to receive a 64 byte packet containing
	// the command in byte[0] and then the numbers 0-62 in bytes 1-63.
	UINT8 bufferPointer;
    UINT8 expectedData;
    UINT8 dataReceivedOk;
    UINT8 dataSentOk;
    byte bComRes;
    static  byte led_cnt = 0;
    static unsigned int ds_cnt =0;
    unsigned char tm[2];
    int iTempr;
    int uiTempr;
    float f_tm;
    unsigned int u16tm=0;
    char number[8];
    int i;
    // Check if we are in the configured state; otherwise just return
    if((USBDeviceState < CONFIGURED_STATE) || (USBSuspendControl == 1))
    {
	   // We are not configured, set LED1 to off to show status
	   // mStatusLED1_off();
       LED0 =1;
	   return;
	}
	// We are configured, show state in LED1
	//mStatusLED1_on()
    if(INTCONbits.TMR0IF)
	{
		INTCONbits.TMR0IF=0;
        led_cnt++;
		if (led_cnt==23)
		{
			LED0 =1;
		}
        if ( led_cnt==46 )
        {
            ds_cnt++;
            led_cnt=0;
            LED0= 0;
        }
        if( 8 == ds_cnt )
        {
        	ds_cnt=0;
            ow_reset();
            Delay100TCYx(30);
            ow_write_byte(DS1820_CMD_SKIPROM );
            Delay100TCYx(1);
            Delay100TCYx(1);
            ow_write_byte( DS1820_CMD_CONVERTTEMP);  
       }                      
    }
	// Check to see if the success indicator is on and update the delay counter
	if (successIndicatorFlag == FLAG_TRUE)
	{
		//mStatusLED2_on();
		successIndicatorCounter++;
		if (successIndicatorCounter == 20000)
		{
			//mStatusLED2_off();
			successIndicatorCounter = 0;
			successIndicatorFlag = FLAG_FALSE;
		}
	}
    // Check to see if the failure indicator is on and update the delay counter
    if (failureIndicatorFlag == FLAG_TRUE)
	{
		//mStatusLED3_on();
		failureIndicatorCounter++;
		if (failureIndicatorCounter == 80000)
		{
			//mStatusLED3_off();
			failureIndicatorCounter = 0;
			failureIndicatorFlag = FLAG_FALSE;
		}
	}
	// Check if data was received from the host.
    if(!HIDRxHandleBusy(USBOutHandle))
    {
	    // Test to see if we are in bulk send/receieve mode, or if we are waiting for a command
	    if (bulkSendFlag == FLAG_TRUE || bulkReceiveFlag == FLAG_TRUE)
	    {
		    // We are either bulk sending or receieving
		    // If we are bulk sending, check that we are not busy and send the next packet
		    if (bulkSendFlag == FLAG_TRUE && !HIDTxHandleBusy(USBInHandle))
		    {
				// Send the next packet
				expectedData = bulkSendPacketCounter;

	            for (bufferPointer = 0; bufferPointer < 64; bufferPointer++)
	            {
		            	ToSendDataBuffer[bufferPointer] = expectedData;
		        }
		        // Transmit the response to the host
                USBInHandle = HIDTxPacket(HID_EP,(BYTE*)&ToSendDataBuffer[0],64);
				bulkSendPacketCounter++; // Next packet
				// Are we done yet?
				if (bulkSendPacketCounter == bulkSendExpectedPackets)
				{
					// All done, indicate success and go back to command mode
					bulkSendFlag = FLAG_FALSE;
					successIndicatorFlag = FLAG_TRUE;
				}
			}
			// If we are bulk receiving get the next packet
			if (bulkReceiveFlag == FLAG_TRUE)
			{
				// The received data buffer is already filled by the USB stack
				// we just have to confirm the data integrity
				expectedData = bulkReceivePacketCounter;
				for (bufferPointer = 0; bufferPointer < 64; bufferPointer++)
				{
					// If the data isn't what we expected, turn on the failure light
					if (ReceivedDataBuffer[bufferPointer] != expectedData)
						failureIndicatorFlag = FLAG_TRUE;
				}
				bulkReceivePacketCounter++;
				// Are we done yet?
				if (bulkReceivePacketCounter == bulkReceiveExpectedPackets)
				{
					// All done, indicate success and go back to command mode
					bulkReceiveFlag = FLAG_FALSE;
					successIndicatorFlag = FLAG_TRUE;
				}
			}
		}
		else
		{
			// Command mode
            bComRes = 0;
	        switch(ReceivedDataBuffer[0])
			{
            	case 1:   
					LED1 = ReceivedDataBuffer[1];
                    break;                                     
                case 2: 
					LED2 = ReceivedDataBuffer[1];
                    break;                                       
                case 3: 
					LED3 = ReceivedDataBuffer[1];
                    break;                                       
                case 4: 
					LED4 = ReceivedDataBuffer[1];
                    break;                                       
				case 5: 
					LED5 = ReceivedDataBuffer[1];
                    break;                                       
				case 6: 
					LED6 = ReceivedDataBuffer[1];
                    break;                                       
				case 7:
					LED7 = ReceivedDataBuffer[1];
                    break;                                       
				case 8:
					LED8 = ReceivedDataBuffer[1];
                    break;
				case 9:
					LED9 = ReceivedDataBuffer[1];
                    break;
				case 10: 
					LED10 = ReceivedDataBuffer[1];
                    break;                                       
				case 11:  
					LED11 = ReceivedDataBuffer[1];
                    break;                                      
				case 12: 
                    LED12 = ReceivedDataBuffer[1];
                    break;
                case 13:                                          
                    ow_reset();
                    Delay100TCYx(30);
                    ow_write_byte(DS1820_CMD_SKIPROM);
                    Delay100TCYx(1);
                    Delay100TCYx(1);
                    Delay100TCYx(1);
                    ow_write_byte(DS1820_CMD_READSCRPAD);
                    Delay10TCYx(1);
                    Delay100TCYx(1);
                    ToSendDataBuffer[1]=ow_read_byte();
                    Delay100TCYx(1);
                    Delay10TCYx(1);
                    ToSendDataBuffer[0]=ow_read_byte();
                    ow_reset();
                    break;
                case 14:
                    ow_reset();
                    Delay100TCYx(30);
                    ow_write_byte(DS1820_CMD_READROM );
                    for( i=0; i<8; i++ )
                    {
                    	Delay100TCYx(1);
                        number[i] = ow_read_byte();
                    }
                    ow_reset();
                    memcpy( ToSendDataBuffer, number,  8  );
                    ToSendDataBuffer[8] = 0;
                    break;
	            default:	// Unknown command received
                    bComRes = 1;	           		
			}
            printf( " ReceivedDataBuffer[0] = %hhX",  ReceivedDataBuffer[0]);               
            if( 1 == bComRes  )
            {
                sprintf( ToSendDataBuffer, "UNKNOWN COMMAND");
            }
            else
            {
				if (ReceivedDataBuffer[0] <= (unsigned char)MAX_LED_VALUE)
				{
                	sprintf( ToSendDataBuffer, "OK");
				}
            }
            // Transmit the response to the host
		    if(!HIDTxHandleBusy(USBInHandle))
			{
				USBInHandle = HIDTxPacket(HID_EP,(BYTE*)&ToSendDataBuffer[0],64);
			}
		}
		// Only re-arm the OUT endpoint if we are not bulk sending
		if (bulkSendFlag == FLAG_FALSE)
		{
	        // Re-arm the OUT endpoint for the next packet
	        USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);
  		}
  	}
}

// USB Callback handling routines -----------------------------------------------------------

// Call back that is invoked when a USB suspend is detected
void USBCBSuspend(void)
{
}

// This call back is invoked when a wakeup from USB suspend is detected.
void USBCBWakeFromSuspend(void)
{
}

// The USB host sends out a SOF packet to full-speed devices every 1 ms.
void USBCB_SOF_Handler(void)
{
    // No need to clear UIRbits.SOFIF to 0 here. Callback caller is already doing that.
}

// The purpose of this callback is mainly for debugging during development.
// Check UEIR to see which error causes the interrupt.
void USBCBErrorHandler(void)
{
    // No need to clear UEIR to 0 here.
    // Callback caller is already doing that.
}

// Check other requests callback
void USBCBCheckOtherReq(void)
{
    USBCheckHIDRequest();
}

// Callback function is called when a SETUP, bRequest: SET_DESCRIPTOR request arrives.
void USBCBStdSetDscHandler(void)
{
    // You must claim session ownership if supporting this request
}

//This function is called when the device becomes initialized
void USBCBInitEP(void)
{
    // Enable the HID endpoint
    USBEnableEndpoint(HID_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    // Re-arm the OUT endpoint for the next packet
    USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);
}

// Send resume call-back
void USBCBSendResume(void)
{
    static WORD delay_count;    
    USBResumeControl = 1;                // Start RESUME signaling    
    delay_count = 1800U;                // Set RESUME line for 1-13 ms
    do
    {
        delay_count--;
    }while(delay_count);
    USBResumeControl = 0;
}

// USB callback function handler
BOOL USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, WORD size)
{
    switch(event)
    {
        case EVENT_TRANSFER:
            // Application callback tasks and functions go here
            break;
        case EVENT_SOF:
            USBCB_SOF_Handler();
            break;
        case EVENT_SUSPEND:
            USBCBSuspend();
            break;
        case EVENT_RESUME:
            USBCBWakeFromSuspend();
            break;
        case EVENT_CONFIGURED:
            USBCBInitEP();
            break;
        case EVENT_SET_DESCRIPTOR:
            USBCBStdSetDscHandler();
            break;
        case EVENT_EP0_REQUEST:
            USBCBCheckOtherReq();
            break;
        case EVENT_BUS_ERROR:
            USBCBErrorHandler();
            break;
        default:
            break;
    }
    return FLAG_TRUE;
}